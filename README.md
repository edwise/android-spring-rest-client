Proyecto montado con Android Studio (con gradle), con los siguientes frameworks / libraries / funcionalidades:

- Spring for Android

- Uso de HATEOAS


Requisitos:

- Gradle instalado

- Proyecto "Complete Spring Project" arrancado y funcionando.


Fuentes:

- http://spring.io/guides/gs/consuming-rest-android/

- http://blog.safaribooksonline.com/2013/10/11/building-smarter-clients-with-springs-resttemplate/


