package com.edwise.androidspringrestclient.app;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.edwise.androidspringrestclient.app.assemblers.BookResource;
import com.edwise.androidspringrestclient.app.entities.Book;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Random;

public class MainActivity extends ActionBarActivity {
    private static String LOG_TAG = MainActivity.class.toString();

    private static final String REST_SERVICE_URL = "http://localhost:8080/api/book/";
    private static final int MAX_BOOK_ID = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        new HttpRequestTask().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            new HttpRequestTask().execute();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }


    private class HttpRequestTask extends AsyncTask<Void, Void, Book> {
        @Override
        protected Book doInBackground(Void... params) {
            Book book = null;
            // Obtenemos un libro con id aleatorio, para que vaya cambiando al pulsar "refresh"
            int bookId = getRandomBookId();
            Log.i(LOG_TAG, "Book al que accedemos: " + bookId);
            try {
                final String url = REST_SERVICE_URL + bookId;
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                ResponseEntity<BookResource> bookResourceResponseEntity = restTemplate.getForEntity(url, BookResource.class);
                BookResource bookResource = bookResourceResponseEntity.getBody();
                book = bookResource.book;
            } catch (Exception e) {
                Log.e(LOG_TAG, "Error al acceder al book con id " + bookId, e);
            }

            return book;
        }

        @Override
        protected void onPostExecute(Book book) {
            TextView bookIdText = (TextView) findViewById(R.id.id_value);
            TextView bookTitleText = (TextView) findViewById(R.id.content_value);
            TextView bookIsbnText = (TextView) findViewById(R.id.isbn_value);
            TextView bookReleaseDateText = (TextView) findViewById(R.id.release_date_value);
            TextView bookAuthorsText = (TextView) findViewById(R.id.authors_value);
            if (book != null) {
                bookIdText.setText(book.getId().toString());
                bookTitleText.setText(book.getTitle());
                bookIsbnText.setText(book.getIsbn());
                bookReleaseDateText.setText(book.getReleaseDate().toString());
                bookAuthorsText.setText(book.getAuthors().toString());
            } else {
                Log.i(LOG_TAG, "No existe el book, mostramos datos vacios.");

                bookIdText.setText("NO EXISTE");
                bookTitleText.setText("-");
                bookIsbnText.setText("-");
                bookReleaseDateText.setText("-");
                bookAuthorsText.setText("-");
            }
        }

        private int getRandomBookId() {
            Random random = new Random();
            return random.nextInt(MAX_BOOK_ID) + 1;
        }
    }

}