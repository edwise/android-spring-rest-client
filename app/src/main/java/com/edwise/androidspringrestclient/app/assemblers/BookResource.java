package com.edwise.androidspringrestclient.app.assemblers;

import com.edwise.androidspringrestclient.app.entities.Book;

import org.springframework.hateoas.ResourceSupport;

public class BookResource extends ResourceSupport {
    public Book book;
}
